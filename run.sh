#!/bin/bash
set -e

CRON_SCRIPT_PATH=${CRON_SCRIPT_PATH}
CRON_SCHEDULE=${CRON_SCHEDULE}
CRON_LOG=${CRON_LOG}
CRON_GRACE_PERIOD_SECONDS=${CRON_GRACE_PERIOD_SECONDS}

if [[ ! -z "${CRON_SCRIPT_PATH}" ]]; then

    # Grace period, after ECS service starts running, in seconds before cron jobs begin executing.
    sleep $CRON_GRACE_PERIOD_SECONDS;

    # Schedule the cron job
    echo "$CRON_SCHEDULE www-data bash ""$CRON_SCRIPT_PATH"" 2>&1 | tee -a ""$CRON_LOG""" | tee --append /etc/crontab

    # Create CRON_LOG file, and adjust permissions
    echo "" >> "$CRON_LOG"
    chgrp -R www-data "$CRON_LOG"
    chmod -R ug+rwx "$CRON_LOG"

    # Run the cron, and watch the CRON_LOG file
    cron
    tail -f "$CRON_LOG"
fi
